resource "aws_instance" "test" {
  count = var.instance_count
  ami = "ami-0245697ee3e07e755"
  instance_type = "t2.medium"
}
variable "instance_count" {
  default = "12"
}
#
